package org.example.use_cases.pari;

import org.example.infrastructure.PariRepositoryInterface;
import org.example.model.pari.*;
import org.example.model.utilisateur.Groupe;
import org.example.model.utilisateur.GroupeService;
import org.example.model.utilisateur.Membre;
import org.example.model.utilisateur.MembreService;

import java.util.List;

public class ProposerUnPariEnGroupe {
    private MatchService matchService;
    private GroupeService groupeService;
    private MembreService membreService;
    private PariRepositoryInterface pariRepositoryInterface;

    public ProposerUnPariEnGroupe(MatchService matchService, GroupeService groupeService, MembreService membreService, PariRepositoryInterface pariRepositoryInterface) {
        this.matchService = matchService;
        this.groupeService = groupeService;
        this.membreService = membreService;
        this.pariRepositoryInterface = pariRepositoryInterface;
    }

    public void proposerUnPari(PropostionDTO propostionDTO) {
        Groupe groupe = groupeService.recuepereGroupeParID(propostionDTO.getGroupeID());
        Match match = matchService.recupereMatchParID(propostionDTO.getMatchID());
        Membre membre = membreService.recupereMembreParID(propostionDTO.getMembreID());
        Score score = new Score(propostionDTO.getScoreEquipeDomicile(), propostionDTO.getScoreEquipeExterieur());
        PropositionPari propositionPari = new PropositionPari(match, score);
        Boolean matchExiste = matchService.existe(propositionPari.getMatch());
        Boolean estMembre = groupe.estMembre(membre);
        if(estMembre && matchExiste) {
            List<PropositionPari> propositionParisListe = groupe.proposeUnPari(propositionPari);
            pariRepositoryInterface.sauvegarderUnePropositionDeParis(propositionParisListe);
        }
    }

    public PariRepositoryInterface getPariRepositoryInterface() {
        return pariRepositoryInterface;
    }
}