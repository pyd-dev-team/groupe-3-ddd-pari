package org.example.infrastructure;

import org.example.model.pari.PropositionPari;

import java.util.List;

public interface PariRepositoryInterface {
    void sauvegarderUnePropositionDeParis(List<PropositionPari> paris);

    PropositionPari recupereUneProposition(PropositionPari pari);
}
