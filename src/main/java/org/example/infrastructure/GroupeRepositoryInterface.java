package org.example.infrastructure;

import org.example.model.utilisateur.Groupe;

public interface GroupeRepositoryInterface {
    Groupe recupereGroupeParID(long groupeID);
}
