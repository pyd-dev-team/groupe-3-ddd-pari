package org.example.infrastructure;

import org.example.model.pari.Match;

public interface MatchRepositoryInterface {
    boolean matchExiste(Match match);

    Match recupereMatchParID(long matchId);
}
