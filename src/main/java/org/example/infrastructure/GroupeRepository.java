package org.example.infrastructure;

import org.example.model.utilisateur.Groupe;
import org.example.model.utilisateur.Membre;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GroupeRepository implements GroupeRepositoryInterface {
    public List<Groupe> groupes = new ArrayList(Arrays.asList(
            new Groupe(1, "Equipe 7", Arrays.asList(new Membre(1), new Membre(2), new Membre(3))),
            new Groupe(2, "Equipe 8", Arrays.asList(new Membre(1), new Membre(2), new Membre(3))),
            new Groupe(3, "Equipe 10", Arrays.asList(new Membre(1), new Membre(2), new Membre(3)))
    ));

    @Override
    public Groupe recupereGroupeParID(long groupeID) {
        return groupes.stream().filter((Groupe groupe)-> groupe.getId() == groupeID).findFirst().get();
    }
}
