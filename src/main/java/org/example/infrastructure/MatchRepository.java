package org.example.infrastructure;

import org.example.model.pari.Match;
import org.example.model.pari.Statut;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MatchRepository implements MatchRepositoryInterface {
    public List<Match> matchDisponibles = new ArrayList(Arrays.asList(
            new Match(1, "FC Barcelone", "Real Madrid", Statut.termine),
            new Match(2, "Man City", "PSG", Statut.pasDebute),
            new Match(3, "Juventus", "Bayern", Statut.enCours)
    ));

    @Override
    public boolean matchExiste(Match match) {
        if(!matchDisponibles.contains(match)){
            throw new IllegalArgumentException("Le match n'existe pas");
        }
        return true;
    }

    @Override
    public Match recupereMatchParID(long matchId) {
        return matchDisponibles.stream().filter((Match match)-> match.getID() == matchId).findFirst().get();
    }
}
