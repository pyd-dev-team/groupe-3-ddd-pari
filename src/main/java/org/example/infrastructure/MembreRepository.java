package org.example.infrastructure;

import org.example.model.utilisateur.Membre;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MembreRepository implements MembreRepositoryInterface {
    public List<Membre> membres = new ArrayList(Arrays.asList(
            new Membre(1),
            new Membre(2),
            new Membre(3),
            new Membre(99)
    ));

    public Membre recupereMembreParID(long membreID) {
        return membres.stream().filter((Membre membre)-> membre.getId() == membreID).findFirst().get();
    }
}
