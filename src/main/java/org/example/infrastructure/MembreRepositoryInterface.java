package org.example.infrastructure;

import org.example.model.utilisateur.Membre;

public interface MembreRepositoryInterface {
    Membre recupereMembreParID(long membreID);
}
