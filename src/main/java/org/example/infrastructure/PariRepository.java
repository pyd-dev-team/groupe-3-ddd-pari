package org.example.infrastructure;

import org.example.model.pari.PropositionPari;

import java.util.ArrayList;
import java.util.List;

public class PariRepository implements PariRepositoryInterface{
    public List<PropositionPari> propositionParisListe = new ArrayList();

    @Override
    public void sauvegarderUnePropositionDeParis(List<PropositionPari> propositionParisListe){
        this.propositionParisListe = propositionParisListe;
    }

    @Override
    public PropositionPari recupereUneProposition(PropositionPari pari) {
        return propositionParisListe.stream().filter((PropositionPari p)-> p.getId() == pari.getId()).findFirst().get();
    }
}
