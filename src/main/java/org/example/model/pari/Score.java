package org.example.model.pari;

import java.util.Objects;

public class Score {
    private int nombreDeButEquipeDomicle;
    private int nombreDeButEquipeExterieur;

    public Score(int nombreDeButEquipeDomicle, int nombreDeButEquipeExterieur) {
        if(nombreDeButEquipeDomicle < 0 || nombreDeButEquipeExterieur < 0){
            throw new IllegalArgumentException("Le score ne peut pas être négatif");
        }
        this.nombreDeButEquipeDomicle = nombreDeButEquipeDomicle;
        this.nombreDeButEquipeExterieur = nombreDeButEquipeExterieur;
    }

    public int getNombreDeButEquipeDomicle() {
        return nombreDeButEquipeDomicle;
    }

    public int getNombreDeButEquipeExterieur() {
        return nombreDeButEquipeExterieur;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Score score = (Score) o;
        return nombreDeButEquipeDomicle == score.nombreDeButEquipeDomicle && nombreDeButEquipeExterieur == score.nombreDeButEquipeExterieur;
    }

    @Override
    public int hashCode() {
        return Objects.hash(nombreDeButEquipeDomicle, nombreDeButEquipeExterieur);
    }
}
