package org.example.model.pari;

import org.example.infrastructure.MatchRepositoryInterface;

public class MatchService {
    private MatchRepositoryInterface matchRepository;

    public MatchService(MatchRepositoryInterface matchRepository) {
        this.matchRepository = matchRepository;
    }

    public boolean existe(Match match) {
        return matchRepository.matchExiste(match);
    }

    public Match recupereMatchParID(long matchID){
        return matchRepository.recupereMatchParID(matchID);
    }
}
