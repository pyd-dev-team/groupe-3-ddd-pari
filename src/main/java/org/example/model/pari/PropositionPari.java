package org.example.model.pari;

import java.util.Objects;

public class PropositionPari {
    private int id;
    private Match match;
    private Score score;

    public PropositionPari(Match match, Score score){
        if(match.getStatut() == Statut.termine){
            throw new IllegalArgumentException("Impossible de proposer un pari sur un match terminé");
        }
        this.match = match;
        this.score = score;
    }

    public int getId() {
        return id;
    }

    public Match getMatch() {
        return match;
    }

    public Score getScore() {
        return score;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PropositionPari that = (PropositionPari) o;
        return id == that.id;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }
}
