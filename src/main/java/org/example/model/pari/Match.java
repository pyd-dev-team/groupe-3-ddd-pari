package org.example.model.pari;


import java.util.Objects;

public class Match {
    private long matchID;
    private String equipeDomicle;
    private String equipeExterieure;
    private Statut statut;

    public Match(long matchID, String equipeDomicle, String equipeExterieure, Statut statut) {
        if(equipeDomicle == "" || equipeExterieure == ""){
            throw new IllegalArgumentException("Une equipe ne peut pas etre vide");
        }
        this.matchID = matchID;
        this.equipeDomicle = equipeDomicle;
        this.equipeExterieure = equipeExterieure;
        this.statut = statut;
    }

    public long getID() {
        return matchID;
    }

    public Statut getStatut(){
        return this.statut;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Match match = (Match) o;
        return matchID == match.matchID && Objects.equals(equipeDomicle, match.equipeDomicle) && Objects.equals(equipeExterieure, match.equipeExterieure) && statut == match.statut;
    }

    @Override
    public int hashCode() {
        return Objects.hash(matchID, equipeDomicle, equipeExterieure, statut);
    }
}
