package org.example.model.pari;

public class PropostionDTO {
    private long membreID;
    private long groupeID;
    private long matchID;
    private int scoreEquipeDomicile;
    private int scoreEquipeExterieur;

    public PropostionDTO(long membreID, long groupeID, long matchID, int scoreEquipeDomicile, int scoreEquipeExterieur) {
        this.membreID = membreID;
        this.groupeID = groupeID;
        this.matchID = matchID;
        this.scoreEquipeDomicile = scoreEquipeDomicile;
        this.scoreEquipeExterieur = scoreEquipeExterieur;
    }

    public int getScoreEquipeDomicile() {
        return scoreEquipeDomicile;
    }

    public int getScoreEquipeExterieur() {
        return scoreEquipeExterieur;
    }

    public long getMatchID() {
        return matchID;
    }

    public long getMembreID() {
        return membreID;
    }

    public long getGroupeID() {
        return groupeID;
    }
}
