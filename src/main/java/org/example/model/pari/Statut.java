package org.example.model.pari;

public enum Statut{
    pasDebute,
    enCours,
    termine
}
