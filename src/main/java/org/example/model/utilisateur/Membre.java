package org.example.model.utilisateur;

import java.util.Objects;

public class Membre {
    private long id;

    public Membre(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Membre membre = (Membre) o;
        return id == membre.id;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }
}
