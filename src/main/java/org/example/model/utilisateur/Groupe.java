package org.example.model.utilisateur;

import org.example.model.pari.PropositionPari;

import java.util.ArrayList;
import java.util.List;

public class Groupe {
    private Long id;
    private String nom;
    private List<Membre> membres;

    public Groupe(long id, String nom, List<Membre> membres) {
        if(membres.isEmpty()){
            throw new IllegalArgumentException("Le groupe ne peut pas etre vide");
        }
        this.id = id;
        this.nom = nom;
        this.membres = membres;
    }

    public Long getId() {
        return id;
    }

    public List<Membre> getMembres() {
        return this.membres;
    }

    public String getNom() {
        return this.nom;
    }

    public List<PropositionPari> proposeUnPari(PropositionPari propositionPari) {
        List<PropositionPari> propositionParListe = new ArrayList<>();
        for(Membre membre : this.membres){
            propositionParListe.add(propositionPari);
        }
        return propositionParListe;
    }

    public boolean estMembre(Membre membre) {
        if(!membres.contains(membre)) {
            throw new IllegalArgumentException("L'utilisateur n'est pas membre du groupe");
        }
        return true;
    }
}
