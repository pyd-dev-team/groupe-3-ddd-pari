package org.example.model.utilisateur;

import org.example.infrastructure.GroupeRepositoryInterface;

public class GroupeService {
    private GroupeRepositoryInterface groupeRepository;

    public GroupeService(GroupeRepositoryInterface groupeRepository){
        this.groupeRepository = groupeRepository;
    }

    public Groupe recuepereGroupeParID(long groupeID) {
        return this.groupeRepository.recupereGroupeParID(groupeID);
    }
}
