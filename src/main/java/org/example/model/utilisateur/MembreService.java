package org.example.model.utilisateur;

import org.example.infrastructure.MembreRepositoryInterface;

public class MembreService {
    private MembreRepositoryInterface membreRepository;

    public MembreService(MembreRepositoryInterface membreRepository) {
        this.membreRepository = membreRepository;
    }

    public Membre recupereMembreParID(long membreID){
        return membreRepository.recupereMembreParID(membreID);
    }
}
