package org.example.use_cases.pari;

import org.example.infrastructure.GroupeRepository;
import org.example.infrastructure.MatchRepository;
import org.example.infrastructure.MembreRepository;
import org.example.infrastructure.PariRepository;
import org.example.model.pari.*;
import org.example.model.utilisateur.GroupeService;
import org.example.model.utilisateur.MembreService;

import static org.junit.jupiter.api.Assertions.*;

class ProposerUnPariEnGroupeTest {

    @org.junit.jupiter.api.Test
    void neDevraitPasProposerUnPariSurUnMatchTermine() {
        assertThrows(IllegalArgumentException.class, ()-> {
            PropositionPari pariPropostion = new PropositionPari(new Match(3, "Juventus", "Bayern", Statut.enCours), new Score(0, 0));
            PropostionDTO propostionDTO = new PropostionDTO(1, 1, 1, 0, 0);
            ProposerUnPariEnGroupe proposerUnPariEnGroupe = new ProposerUnPariEnGroupe(
                    new MatchService(new MatchRepository()),
                    new GroupeService(new GroupeRepository()),
                    new MembreService(new MembreRepository()),
                    new PariRepository());
            proposerUnPariEnGroupe.proposerUnPari(propostionDTO);
            assertEquals(proposerUnPariEnGroupe.getPariRepositoryInterface().recupereUneProposition(pariPropostion), pariPropostion);
        });
    }

    @org.junit.jupiter.api.Test
    void devraitProposerUnPariSurUnMatchEnCours() {
        assertDoesNotThrow(()-> {
            PropostionDTO propostionDTO = new PropostionDTO(1, 1, 3, 0, 0);
            PropositionPari pariPropostion = new PropositionPari(new Match(3, "Juventus", "Bayern", Statut.enCours), new Score(0, 0));
            ProposerUnPariEnGroupe proposerUnPariEnGroupe = new ProposerUnPariEnGroupe(
                    new MatchService(new MatchRepository()),
                    new GroupeService(new GroupeRepository()),
                    new MembreService(new MembreRepository()),
                    new PariRepository());
            proposerUnPariEnGroupe.proposerUnPari(propostionDTO);
            assertEquals(proposerUnPariEnGroupe.getPariRepositoryInterface().recupereUneProposition(pariPropostion), pariPropostion);
        });
    }

    @org.junit.jupiter.api.Test
    void neDevraitPasProposerUnPariPourUnUtilisateurNonMembre() {
        assertThrows(IllegalArgumentException.class, ()-> {
            PropositionPari pariPropostion = new PropositionPari(new Match(3, "Juventus", "Bayern", Statut.enCours), new Score(0, 0));
            PropostionDTO propostionDTO = new PropostionDTO(99, 1, 1, 0, 0);
            ProposerUnPariEnGroupe proposerUnPariEnGroupe = new ProposerUnPariEnGroupe(
                    new MatchService(new MatchRepository()),
                    new GroupeService(new GroupeRepository()),
                    new MembreService(new MembreRepository()),
                    new PariRepository());
            proposerUnPariEnGroupe.proposerUnPari(propostionDTO);
        });
    }

    @org.junit.jupiter.api.Test
    void neDevraitPasProposerUnPariPourUnScoreNegatif() {
        assertThrows(IllegalArgumentException.class, ()-> {
            PropositionPari pariPropostion = new PropositionPari(new Match(3, "Juventus", "Bayern", Statut.enCours), new Score(0, 0));
            PropostionDTO propostionDTO = new PropostionDTO(1, 1, 1, -1, 0);
            ProposerUnPariEnGroupe proposerUnPariEnGroupe = new ProposerUnPariEnGroupe(
                    new MatchService(new MatchRepository()),
                    new GroupeService(new GroupeRepository()),
                    new MembreService(new MembreRepository()),
                    new PariRepository());
            proposerUnPariEnGroupe.proposerUnPari(propostionDTO);
        });
    }
}